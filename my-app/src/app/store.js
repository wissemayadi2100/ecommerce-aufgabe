import {combineReducers, configureStore} from "@reduxjs/toolkit";
import { productApi } from '../service/productApi'
import logger from 'redux-logger'
import {setupListeners} from "@reduxjs/toolkit/query";

export const rootReducers = combineReducers({
  [productApi.reducerPath]: productApi.reducer,
})



export const store = configureStore({
    reducer: {
        // Add the generated reducer as a specific top-level slice
        [productApi.reducerPath]: productApi.reducer
    },
    // Adding the api middleware enables caching, invalidation, polling,
    // and other useful features of `rtk-query`.
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware(

        ).concat(productApi.middleware).concat(logger),
})

setupListeners(store.dispatch)