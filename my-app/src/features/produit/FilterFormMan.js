import React, {useEffect, useRef, useState} from 'react';
import {useDispatch} from "react-redux";
import {productApi, useGetProductsByFilterQuery} from "../../service/productApi";
import {Link, useLocation, useNavigate} from "react-router-dom";


//this page willl filter the product for each gender and display the filtered product query params
const FilterFormMan = () => {
    const dispatch = useDispatch()
    const location = useLocation();
    const navigate = useNavigate();
    const [country, setCountry] = useState('')
    const [gender, setGender] = useState('')
    const [web_category, setWeb_category] = useState('');
    const [page, setPage] = useState(1);
    const imageApi = "https://api.newyorker.de/csp/images/image/public/"
    const resolution = "mid";
    const frame = "frame=2_3";
    const bottomRef = useRef(null);
    const scrollToBottom=()=>
    {
        bottomRef.current.scrollIntoView({ behavior: 'smooth' });
    }
    const handleSubmit = (e) =>
    {
        e.preventDefault()
        dispatch(productApi.endpoints.getProductsByFilter.initiate({filter:country, gender, web_category}))

    }

    useEffect(() =>
    {
        const params = new URLSearchParams()
        if (country)
        {
            params.append("country", country);
        }
        else
        {
            params.delete("country")
        }
        if(gender)
        {
            params.append("gender",gender)
        }
        else
        {
            params.delete("gender")
        }
        if(web_category)
        {
            params.append("web_category",web_category)
        }
        else
        {
            params.delete("web_category")
        }

        navigate({search: params.toString()})
    }, [country,gender,web_category, navigate])

    useEffect(() =>
    {
        if (location.pathname.includes("/men"))
        {
            localStorage.setItem("gender", "MALE");
        }
        else
        {
            localStorage.setItem("gender", "FEMALE");
        }
    }, [location]);



    const {data, isLoading, isError, refetch,isFetching} = useGetProductsByFilterQuery({
        country,
        gender,
        web_category,

    },{
        refetchOnMountOrArgChange: true,
        refetchOnMount: true,
        keepPreviousData: true,
    });

    //clear recherche filter button
    const onClearFilter = (e) =>
    {
        setWeb_category("")
        localStorage.setItem('web_category', "");
    }


    useEffect(() =>
    {
        setCountry(window.localStorage.getItem('country'));
        setGender(window.localStorage.getItem('gender'));
    }, [country,gender]);


    if (isLoading)
    {
        return <div>Loading...</div>
    }

    if (!data)
    {
        return <div>Something went wrong ...</div>
    }


    return (
        <div className="ml-2 mt-24" ref={bottomRef}>
            <form className="flex mb-2" onSubmit={handleSubmit}>
                <div className="flex-none mx-5">
                    <label>country</label>
                    <select
                        id="location"
                        name="location"
                        className="mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        value={country}
                        onChange={(e) => {
                            setCountry(e.target.value);
                            localStorage.setItem("country", e.target.value);
                        }}>
                        <option></option>
                        <option value="de">Germany</option>
                        <option value="fr">France</option>
                    </select>
                </div>
                <div className="flex-none  mx-2">
                    <label>gender</label>
                    <select
                        id="gender"
                        name="gender"
                        className=" mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        value={gender}
                        onChange={(e) => {
                            setGender(e.target.value);
                            localStorage.setItem("gender", e.target.value);
                        }}
                        disabled={localStorage.getItem("gender") === "MALE"}>
                        <option value="NONE"></option>
                        <option value="MALE">male</option>
                        <option value="">All product</option>
                    </select>
                </div>
                <div className="flex-none  mx-5">
                    <label value="Accessoires">accessoires</label>
                    <select
                        id="web_category"
                        name="web_category"
                        className=" mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        value={web_category}
                        onChange={(e) => {
                            setWeb_category(e.target.value);
                        }}>
                        <option onClick={() => setWeb_category("")}></option>
                        <option value="Accessoires,WCA02306,WCA02303,WCA02308,WCA02309,WCA02307,WCA02301,WCA02302">
                            Accessoires
                        </option>
                        <option value="WCA02306">Gurtel</option>
                        <option value="WCA02303">Mutzen und caps</option>
                        <option value="WCA02308">Schmuck</option>
                        <option value="WCA02309">Sonnenbrillen</option>
                        <option value="WCA02307">Sonntige</option>
                        <option value="WCA02301">Taschen</option>
                        <option value="WCA02302">Tuscher und Schals</option>
                    </select>
                </div>
                <div className="flex-none  mx-2">
                    <label>Hemden</label>
                    <select
                        id="web_category"
                        name="web_category"
                        className=" mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        value={web_category}
                        onChange={(e) => {
                            setWeb_category(e.target.value);
                        }}>
                        <option onClick={() => setWeb_category("")}></option>
                        <option value="WCA02211">Hemden</option>
                    </select>
                </div>
                <div className="flex-none  mx-2">
                    <label>Denim</label>
                    <select
                        id="web_category"
                        name="web_category"
                        className=" mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        value={web_category}
                        onChange={(e) => {
                            setWeb_category(e.target.value);
                        }}>
                        <option onClick={() => setWeb_category("")}></option>
                        <option value="WCA02246,WCA02242,WCA02241,WCA03047,WCA02243,WCA02245,WCA03039,WCA02244,WCA03049">
                            Denim
                        </option>
                        <option value="WCA02241">Slim</option>
                        <option value="WCA02243">Slim Straight</option>
                        <option value="WCA03047">Straight tappered</option>
                        <option value="WCA02242">skinny</option>>
                        <option value="WCA03049">Baggy</option>
                        <option value="WCA02244">Tepered</option>
                        <option value="WCA02246">Regular</option>
                        <option value="WCA03039">Loose</option>
                    </select>
                </div>
                <div className="flex-none  mx-2">
                    <label>T-Shirt</label>
                    <select
                        id="web_category"
                        name="web_category"
                        className=" mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        value={web_category}
                        onChange={(e) => {
                            setWeb_category(e.target.value);
                        }}>
                        <option onClick={() => setWeb_category("")}></option>
                        <option value="WCA00221,WCA00223,WCA00222,WCA00220">T-Shirts</option>
                        <option value="WCA00221">Kurz arm</option>
                        <option value="WCA00223">Lang Arm</option>
                        <option value="WCA00222">Polo shirt</option>
                        <option value="WCA00220">Tank Tops</option>
                    </select>
                </div>
                <div className="flex-none  mx-2">
                    <label>Pullover</label>
                    <select
                        id="web_category"
                        name="web_category"
                        className=" mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        value={web_category}
                        onChange={(e) => {
                            setWeb_category(e.target.value);
                        }}>
                        <option onClick={() => setWeb_category("")}></option>
                        <option value="WCA02222,WCA02224,WCA02221">Sweat & pull over </option>
                        <option value="WCA02222">pull over</option>
                        <option value="WCA02224">strict jacken</option>
                        <option value="WCA02221">sweat shirt</option>
                    </select>
                </div>
                <div className="flex-none  mx-5">
                    <label>Jeans</label>
                    <select
                        id="hosen"
                        name="hosen"
                        className="mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        value={web_category}
                        onChange={(e) => {
                            setWeb_category(e.target.value);
                        }}>
                        <option onClick={() => setWeb_category("")}></option>
                        <option value="WCA02252,WCA02251,WCA02253">Hosen</option>
                        <option value="WCA02252">Jeans</option>
                        <option value="WCA02251">Chinos</option>
                        <option value="WCA02253">Joking hosen</option>
                    </select>
                </div>
                <div className="flex-none  mt-8">
                    <button
                        type="button"
                        onClick={onClearFilter}
                        className="inline-flex items-center gap-x-1.5 rounded-md bg-indigo-600 px-2.5 py-1.5 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">
                        clear filter
                    </button>
                </div>
            </form>
            <div className="bg-white">
                <div className="mx-auto max-w-3xl py-16 px-4 sm:py-24 sm:px-6 lg:px-8">
                    <div className="mt-6 grid grid-cols-1  gap-y-10 sm:grid-cols-3 sm:gap-y-0 sm:gap-x-6 lg:gap-x-8">
                        {data?.items?.map((product, index) => (
                            <div key={product.id} className="group relative">
                                <div className=" overflow-hidden rounded-lg group-hover:opacity-75 sm:aspect-w-2 sm:aspect-h-3 sm:h-auto">
                                    <img
                                        src={`${imageApi}${product.variants[0].images[0].key}?${resolution}&frame=${product.variants[0].images[0].type === 'CUTOUT' ? '1_1' : '2_3'}`}
                                        alt="images"
                                        className="h-full w-full object-cover object-center"
                                    />
                                </div>
                                <h3 className="mt-4 text-base font-semibold text-gray-900">
                                    <Link
                                        to={`/productDetail/${product.id}`}
                                        state={{
                                            product,
                                            key: product.variants[0].images[0].key,
                                            key1: product?.customer_group,
                                        }}
                                    >
                                        <span className="absolute inset-0" />
                                        {product.maintenance_group}
                                    </Link>
                                    {product.variants[0].sale === true ? (
                                        <div className="mt-4 bg-red-700">sales</div>
                                    ) : null}

                                    {product.variants[0].coming_soon === true ? (
                                        <div className="mt-4 bg-blue-900">coming soon</div>
                                    ) : null}

                                    {product.variants[0].new_in === true ? (
                                        <div className="mt-4 bg-green-700">new in</div>
                                    ) : null}

                                </h3>
                                <p className="mt-1 text-sm text-gray-500">
                                    {" "}
                                    {product?.variants[0]?.original_price}{" "}
                                    {product?.variants[0]?.currency}
                                </p>
                            </div>
                        ))}
                        <button onClick={scrollToBottom}>
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-10 h-10">
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M12 19.5v-15m0 0l-6.75 6.75M12 4.5l6.75 6.75"
                                />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>

    );
};

export default FilterFormMan