import React, {useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {productApi, useGetProductsByFilterQuery} from "../../service/productApi";
import {Link, useLocation,  useNavigate} from "react-router-dom";



//this page willl filter the product for each gender and display the filtered product query params
const FilterFormWomen = () => {
    const dispatch = useDispatch()
    const location = useLocation();
    const navigate = useNavigate();
    const [country, setCountry] = useState('')
    const [gender, setGender] = useState('')
    const [web_category, setWeb_category] = useState('');
    const imageApi = "https://api.newyorker.de/csp/images/image/public/"
    const resolution = "higher";
    const frame = "frame=2_3";
    const bottomRef = useRef(null);


    const scrollToBottom=()=> {
        bottomRef.current.scrollIntoView({ behavior: 'smooth' });
    }
    const handleSubmit = (e) => {
        e.preventDefault()
        dispatch(productApi.endpoints.getProductsByFilter.initiate({filter:country, gender, web_category}))
    }

    useEffect(() => {
        if (location.pathname.includes("/women"))
        {
            localStorage.setItem("gender", "FEMALE");
        }

    }, [location]);


    useEffect(() =>
    {
        const params = new URLSearchParams()
        if (country) {
            params.append("country", country);
        }
        else {
            params.delete("country")
        }
        if(gender){
            params.append("gender",gender)
        }
        else
        {
            params.delete("gender")
        }
        if(web_category){
            params.append("web_category",web_category)
        }
        else{
            params.delete("web_category")
        }

        navigate({search: params.toString()})
    }, [country,gender,web_category, navigate])


    const {data, isLoading, isError, refetch,isFetching} = useGetProductsByFilterQuery({
        country,
        gender,
        web_category,

    },{
        refetchOnMountOrArgChange: true,
        refetchOnMount: true,
        keepPreviousData: true,
    });
    const onClearFilter = (e) => {
        setWeb_category("")
        localStorage.setItem('web_category', "");
    }


    useEffect(() => {
        setCountry(window.localStorage.getItem('country'));
        setGender(window.localStorage.getItem('gender'));
    }, [country,gender]);


    if (isLoading) {
        return <div>Loading...</div>
    }

    return (
        <div className="ml-2 mt-24" ref={bottomRef}>
            <form className="flex mb-2" onSubmit={handleSubmit}>
                <div className="flex-none mx-5">
                    <label>country</label>
                    <select
                        id="location"
                        name="location"
                        className="mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        value={country} onChange={(e) => {setCountry(e.target.value)
                        localStorage.setItem('country', e.target.value)}}>
                        <option></option>
                        <option value="de">Germany</option>
                        <option value="be">belguim</option>
                        <option value="fr">France</option>
                    </select>
                </div>
                <div className="flex-none  mx-2">
                    <label>gender</label>
                    <select
                        id="gender"
                        name="gender"
                        className=" mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        value={gender} onChange={(e) => {setGender(e.target.value)
                        localStorage.setItem('gender', e.target.value);
                    }}
                        disabled={localStorage.getItem("gender") ==="FEMALE" ? true : false}
                    >
                        <option></option>
                        <option value="">All products</option>
                        <option value="FEMALE">female</option>
                    </select>
                </div>
                <div className="flex-none  mx-5">
                    <label>Accessoires</label>
                    <select
                        id="web_category"
                        name="web_category"
                        className=" mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"

                        value={web_category} onChange={(e) =>{ setWeb_category(e.target.value)

                        }}

                    >
                        <option value=""></option>
                        <option value="WCA01156,WCA01159,WCA01155,WCA01152,WCA01158,WCA01153,WCA01157,WCA01154">Accessoires
                        </option>
                        <option value="WCA01156">Belt</option>
                        <option value="WCA01155">Hats and caps</option>
                        <option value="WCA01152">Jewellery</option>
                        <option value="WCA01158">Socks</option>
                        <option value="WCA01153">sun glasses</option>
                        <option value="WCA01157">other</option>
                        <option value="WCA01154">Scarves</option>
                        <option value="WCA01159">Gloves</option>

                    </select>
                </div>
                <div className="flex-none  mx-2">
                    <label>Bluse</label>
                    <select
                        id="web_category"
                        name="web_category"
                        className=" mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        value={web_category} onChange={(e) =>{ setWeb_category(e.target.value)
                        // localStorage.setItem('web_category', e.target.value)

                        }}>
                        <option></option>
                        <option value="WCA00122,WCA00121">All Blusen</option>
                        <option value="WCA00122">Blusen & Hemden</option>
                        <option value="WCA00121">Tops</option>
                        <option value="WCA03020">Kurzarm</option>
                        <option value="WCA03021">Lang arm</option>
                    </select>
                </div>
                <div className="flex-none  mx-2">
                    <label>Tops</label>
                    <select
                        id="web_category"
                        name="web_category"
                        className=" mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        value={web_category} onChange={(e) =>{ setWeb_category(e.target.value)
                        }}>
                        <option></option>
                        <option value="WCA00111,WCA00112,WCA00110">All Tops</option>
                        <option value="WCA00111">Kurzarm</option>
                        <option value="WCA00112">Langarm</option>
                        <option value="WCA00110">Tops</option>
                        <option value="WCA03032">Bodies</option>
                        <option value="WCA03033">Kardigan</option>

                    </select>
                </div>
                <div className="flex-none  mx-2">
                    <label>Sweatshirts</label>
                    <select
                        id="web_category"
                        name="web_category"
                        className=" mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        value={web_category} onChange={(e) =>{ setWeb_category(e.target.value)
                       }}>
                        <option></option>
                        <option value="WCA00132,WCA00131">All Sweatshirts</option>
                        <option value="WCA00132">Sweatshirtjacken</option>
                        <option value="WCA00131">Sweatshirts</option>
                    </select>
                </div>
                <div className="flex-none  mx-2">
                    <label>roecke</label>
                    <select
                        id="web_category"
                        name="web_category"
                        className=" mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        value={web_category} onChange={(e) =>{ setWeb_category(e.target.value)

                        }}>
                        <option></option>
                        <option value="WCA00161,WCA00162,WCA00163">roecke</option>
                        <option value="WCA00161">Mini Röcke</option>
                        <option value="WCA00162">Midi Röcke</option>
                        <option value="WCA00163">Maxi Röcke</option>
                        <option value="WCA03074">Shorts</option>
                    </select>
                </div>
                <div className="flex-none  mx-5">
                    <label>Hosen</label>
                    <select
                        id="hosen"
                        name="hosen"
                        className="mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        value={web_category} onChange={(e) =>{ setWeb_category(e.target.value)
                         }}>
                        <option></option>
                        <option value="WCA00172,WCA00173,WCA00171">All Hosen</option>
                        <option value="WCA00172">Hosen</option>
                        <option value="WCA00173">Jogginghosen</option>
                        <option value="WCA00171">Leggings</option>
                    </select>
                </div>
                <div className="flex-none  mt-8">
                    <button
                        type="button"
                        onClick={onClearFilter}
                        className="inline-flex items-center gap-x-1.5 rounded-md bg-indigo-600 px-2.5 py-1.5 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">
                        clear filter

                    </button>
                </div>
            </form>
            <div className="bg-white">
                <div className="mx-auto max-w-3xl py-16 px-4 sm:py-24 sm:px-6 lg:px-8">
                    <div className="mt-6 grid grid-cols-1 gap-y-10 sm:grid-cols-3 sm:gap-y-0 sm:gap-x-6 lg:gap-x-8">
                        {data?.items?.map((product,index) => (

                            <div key={product.id} className="group relative">
                                <div className="h-96 w-full overflow-hidden rounded-lg group-hover:opacity-75 sm:aspect-w-2 sm:aspect-h-3 sm:h-auto">
                                    <img
                                        src={`${imageApi}${product.variants[0].images[0].key}?${resolution}&frame=${product.variants[0].images[0].type === 'CUTOUT' ? '1_1' : '2_3'}`}
                                        alt="images"
                                        className="h-full w-full object-cover object-center"
                                    />
                                </div>
                                <h3 className="mt-4 text-base font-semibold text-gray-900">
                                    <Link to={`/productDetail/${product.id}`} state={{product,key:product.variants[0].images[0].key,key1:product?.customer_group}} >
                                        <span className="absolute inset-0" />
                                        {product.maintenance_group}
                                    </Link>
                                    {product.variants[0].sale === true ? <div className="mt-4 bg-red-700">
                                        sales
                                    </div> : null}
                                    {product.variants[0].coming_soon === true ? <div className="mt-4 bg-blue-900">
                                        coming soon
                                    </div> : null}

                                    {product.variants[0].new_in === true ? <div className="mt-4 bg-green-700">
                                        new in
                                    </div> : null}
                                </h3>
                                <p className="mt-1 text-sm text-gray-500">  {product?.variants[0]?.original_price} {product?.variants[0]?.currency}</p>
                            </div>
                        ))}
                          <button onClick={scrollToBottom}>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-10 h-10">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M12 19.5v-15m0 0l-6.75 6.75M12 4.5l6.75 6.75" />
                            </svg>
                          </button>
                    </div>
                </div>
            </div>
        </div>

    );
};

export default FilterFormWomen;