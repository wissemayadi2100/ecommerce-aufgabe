import React, {useEffect, useState} from 'react'
import {ChevronLeftIcon} from '@heroicons/react/20/solid'
import { RadioGroup } from '@headlessui/react'
import { useLocation, useNavigate} from "react-router-dom";
import {useGetProductIdByCountryQuery, useGetProductImageByIdQuery} from "../../service/productApi";

//page product detail by category and gender
function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}
const  ProductDetailPage=()=> {
    const location = useLocation();
    const navigate = useNavigate();
    //passing product from product page to product detail page
    const {product,key} = location.state;
    //getting product detail by country
    const {data:productById,
        isLoading:productIsLoading,
        isError:productIsError,
        isFetching:productIsFetching} =
        useGetProductIdByCountryQuery({productId:product.id,
            country:product.country});
    //image api by product image key for each variant
    const imageApi = "https://api.newyorker.de/csp/images/image/public/"
    const resolution = "res=mid";
    const frame = "1_1";
    //sales formule
    const discount = Math.round((productById?.variants[0].original_price - productById?.variants[0].current_price) / product.variants[0].original_price *100).toFixed(2);
    const result = Math.trunc(discount);

    // find the default color
    const defaultColor = productById?.variants[0].pantone_color_name;

// set the selected color to the default color
    const [selectedColor, setSelectedColor] = useState(defaultColor);

// find the index of the default color variant
    const defaultVariantIndex = productById?.variants.findIndex(variant => variant.pantone_color_name === defaultColor);

// display the images of the default variant
    const defaultVariant = productById?.variants[defaultVariantIndex];
    // const defaultVariantImages = defaultVariant ? defaultVariant.images : [];

// set the selected color to the default color on page load
    useEffect(() => {
        setSelectedColor(defaultColor);
    }, [defaultColor]);


    //description by country
    const descriptionByCountry = productById?.descriptions.find((description) => {
        if(description.language === localStorage.getItem("country").toUpperCase())
        {
            return description.description
        }
    });

    return (
        <div className="bg-white">
            <div className="pt-6 pb-16 sm:pb-24">
            <nav aria-label="Breadcrumb" className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
              <span className="isolate inline-flex rounded-md shadow-sm">
             <button
                type="button"
                onClick={() =>
                    navigate(-1)}
                className="relative mt-16 inline-flex items-center rounded-l-md bg-white px-2 py-2 text-gray-400 ring-1 ring-inset ring-gray-300 hover:bg-gray-50 focus:z-10">
               <span className="sr-only">Previous</span>
               <ChevronLeftIcon className="h-5 w-5" aria-hidden="true" />
              </button>
              </span>
            </nav>
                <div className="mx-auto mt-8 max-w-2xl px-4 sm:px-6 lg:max-w-7xl lg:px-8">
                    <div className="lg:grid lg:auto-rows-min lg:grid-cols-12 lg:gap-x-8">
                        <div className="lg:col-span-5 lg:col-start-8">
                            <div className="flex justify-items-start">
                                {productById?.variants[0].new_in === true ? (
                                    <h1 className="text-2xl mb-5 text-white bg-gray-600 font-medium text-gray-900">New in</h1>

                                ) : null}
                            </div>
                            <div className="flex justify-items-start">
                                {productById?.variants[0].coming_soon === true ? (
                                    <h1 className="text-2xl mb-5 text-white bg-gray-600 font-medium text-gray-900">Coming soon</h1>

                                ) : null}
                            </div>
                            <div className="flex justify-between">
                                <h1 className="text-xl font-medium text-gray-900">{descriptionByCountry?.description}</h1>
                                <p className="text-xl font-medium text-gray-900">{productById?.variants[0]?.original_price} {productById?.variants[0]?.currency}</p>
                            </div>
                            <hr className="mt-5"/>
                            <div className="flex justify-between mt-5">
                                <h1 className="text-xl font-medium text-gray-900">{productById?.brand}</h1>
                                <h1 className="text-xl font-medium text-gray-900">{productById?.variants[0].product_id}</h1>
                            </div>
                            <hr className="mt-5"/>
                            <div className="flex justify-between mt-5">
                                <h2 className="text-xl font-medium text-gray-900">product basic color</h2>
                                <h1 className="text-xl font-medium text-gray-900">{productById?.variants[0].color_group}</h1>
                            </div>
                            <hr className="mt-5"/>
                            <div className="flex justify-between mt-5">
                                <div>

                                    <div>
                                        <h2 className="text-xl font-medium text-gray-900 flex justify-items-start">colors product</h2>
                                        <fieldset className="mt-4">
                                            <legend className="sr-only">colors choose</legend>
                                            <div className="flex flex-row justify-between flex-wrap">
                                                {productById?.variants.map((variant,index) => (
                                                    <div key={variant.color_name}>
                                                        <label className="">
                                                            <input
                                                                key={variant}
                                                                type="radio"
                                                                name="color"
                                                                value={variant.pantone_color_name}
                                                                checked={selectedColor === variant.pantone_color_name}
                                                                onChange={() => setSelectedColor(variant.pantone_color_name)}
                                                                className="h-4 w-4 mx-2 border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                                            />
                                                            {variant.pantone_color_name}
                                                        </label>
                                                    </div>
                                                ))}
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>

                            </div>
                        </div>
                        {/* Image by color product */}
                        <div className="mt-8 lg:col-span-7 lg:col-start-1 lg:row-span-3 lg:row-start-1 lg:mt-0">
                            <h2 className="sr-only">Images</h2>
                            {productById?.variants.map((variant)=>
                                <div>
                            {selectedColor === variant.pantone_color_name && (
                                <div className="grid grid-cols-1 lg:grid-cols-2 lg:grid-rows-3 lg:gap-8">
                                    {variant.images.map((image,index) => (
                                        console.log(image.type),
                                        <div key={image.key}>
                                            <img src={`${imageApi}${image.key}?${resolution}&frame=${image.type === 'OUTFIT_IMAGE' ? '2_3' : '1_1'}`} />
                                        </div>
                                    ))}
                                </div>
                                )}
                                </div>
                            )}

                        </div>
                        <div className="mt-8 lg:col-span-5">
                            <form>
                                {productById?.variants[0].sale === true ?
                                    <div className="mt-4 bg-red-700">
                                        sales {result} %
                                        <h1 className="text-2xl"> {productById?.variants[0].current_price} {productById?.variants[0]?.currency}</h1>
                                    </div>
                                    : null}
                                {/* Size picker */}
                                <div className="mt-8">
                                    <div className="flex items-center justify-between">
                                        <h2 className="text-sm font-medium text-gray-900">Verfugbar Size</h2>
                                        <a href="my-app/src/features/productDetail#" className="text-sm font-medium text-indigo-600 hover:text-indigo-500">
                                            See sizing chart
                                        </a>
                                    </div>
                                    <RadioGroup className="mt-2">
                                        <RadioGroup.Label className="sr-only"> Choose a size </RadioGroup.Label>
                                        <div className="grid grid-cols-3 gap-3 sm:grid-cols-6">
                                            {productById?.variants && productById?.variants[0]?.sizes.map((size,index) => (
                                                <RadioGroup.Option
                                                    key={size.size_name}
                                                    value={size}
                                                    className={({ active, checked }) =>
                                                        classNames(
                                                            size.size_name ? 'cursor-pointer focus:outline-none' : 'cursor-not-allowed opacity-25',
                                                            active ? 'ring-2 ring-indigo-500 ring-offset-2' : '',
                                                            checked
                                                                ? 'border-transparent bg-indigo-600 text-white hover:bg-indigo-700'
                                                                : 'border-gray-200 bg-white text-gray-900 hover:bg-gray-50',
                                                            'flex items-center justify-center rounded-md border py-3 px-3 text-sm font-medium uppercase sm:flex-1'
                                                        )
                                                    }
                                                >
                                                    <RadioGroup.Label as="span">{size.size_name}</RadioGroup.Label>
                                                </RadioGroup.Option>
                                            ))}
                                        </div>
                                    </RadioGroup>
                                </div>
                                <button
                                    type="submit"
                                    className="mt-8 flex w-full items-center justify-center rounded-md border border-transparent bg-indigo-600 py-3 px-8 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
                                    Add to cart
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default  ProductDetailPage
