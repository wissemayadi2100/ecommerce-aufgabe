import React, {useEffect, useRef, useState} from 'react';
import { useGetAllProductsQuery } from "../../service/productApi";
import {Link, useLocation,  useNavigate, useParams} from "react-router-dom";
import localStorage from "redux-persist/es/storage";



const Products = () => {
    const location = useLocation();
    const navigate = useNavigate();
    const [country, setCountry] = useState("")
    const [page, setPage] = useState(1)
    const [gender, setGender] = useState('')
    const imageApi = "https://api.newyorker.de/csp/images/image/public/"
    const resolution = "res=high";
    const bottomRef = useRef(null);


    const scrollToBottom=()=>
    {
        bottomRef.current.scrollIntoView({ behavior: 'smooth' });
    }

    useEffect(() => {
        if (location.pathname.includes("/allproduct"))
        {
            localStorage.getItem("country", country);
            // localStorage.setItem("gender","NONE")
        }
        // si le country in local storage est different de country dans le url alors on change le country dans le local storage

    }, [location]);

    useEffect(() => {
        const params = new URLSearchParams()
        if (country)
        {
            params.append("country", country);
        }
        else
        {
            params.delete("country")
        }


        navigate({search: params.toString()})

    }, [country,gender])


    //onchange page delete the url params

    const {data, isLoading, isError, refetch,isFetching} = useGetAllProductsQuery({
        country,
        // gender,

    },{
        refetchOnMountOrArgChange: true,


    });


    useEffect(() =>
    {
        setCountry(window.localStorage.getItem('country'));
        setGender(window.localStorage.getItem('gender'))

    }, [country,gender]);

    if (isLoading)
    {
        return <div>Loading...</div>
    }

    return (
        <div className="ml-2" ref={bottomRef}>
            <div className="bg-white">
                <div className="mx-auto max-w-3xl py-16 px-4 sm:py-24 sm:px-6 lg:px-8">
                    <div className="mt-6 grid grid-cols-1 gap-y-10 sm:grid-cols-3 sm:gap-y-0 sm:gap-x-6 lg:gap-x-8">
                        {data?.items?.map((product, index) => (
                            <div key={product.id} className="group relative">
                                <div className="h-96 w-full overflow-hidden rounded-lg group-hover:opacity-75 sm:aspect-w-2 sm:aspect-h-3 sm:h-auto">
                                    <img
                                        src={`${imageApi}${product.variants[0].images[0].key}?${resolution}&frame=${product.variants[0].images[0].type === 'CUTOUT' ? '1_1' : '2_3'}`}
                                        alt="images"
                                        className="h-full w-full object-cover object-center"
                                    />

                                </div>
                                <h3 className="mt-4 text-base font-semibold text-gray-900">
                                    <Link
                                        to={`/productDetail/${product.id}`}
                                        state={{ product, key: product.variants[0].images[0].key }}>
                                        <span className="absolute inset-0" />
                                        {product.maintenance_group}
                                    </Link>
                                    {product.variants[0].sale === true ? (
                                        <div className="mt-4 bg-red-700">sales</div>
                                    ) : null}
                                    {product.variants[0].coming_soon === true ? (
                                        <div className="mt-4 bg-blue-900">coming soon</div>
                                    ) : null}

                                    {product.variants[0].new_in === true ? (
                                        <div className="mt-4 bg-green-700">new in</div>
                                    ) : null}
                                </h3>
                                <p className="mt-1 text-sm text-gray-500">
                                    {" "}
                                    {product?.variants[0]?.original_price}{" "}
                                    {product?.variants[0]?.currency}
                                </p>
                            </div>
                        ))}
                        <button onClick={scrollToBottom}>
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-10 h-10">
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M12 19.5v-15m0 0l-6.75 6.75M12 4.5l6.75 6.75"
                                />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Products;