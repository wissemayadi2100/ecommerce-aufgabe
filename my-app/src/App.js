import React, {useState} from 'react';
import './App.css';
import {Route, Routes} from "react-router-dom";
import ProductDetailPage from "./features/productDetail/ProductDetailPage";
import FilterFormMan from "./features/produit/FilterFormMan";
import FilterFormWomen from "./features/produit/FilterFormWomen";
import Products from "./features/allProduct/Products";
import Navigation from "./component/navbar/navigation";

function App() {

  return (
    <div className="App">
        <Navigation/>
        <Routes>
            <Route>
                <Route path="/productDetail/:productId" element={<ProductDetailPage />} />
                <Route path="/men" element={< FilterFormMan  />} />
                <Route path="/women" element={<FilterFormWomen />} />
                <Route path="/allproduct" element={<Products />} />
            </Route>
        </Routes>
    </div>
  );
}

export default App;
