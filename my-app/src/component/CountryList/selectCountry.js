import React, {useEffect, useState} from 'react';
import {useLocation, useNavigate} from "react-router-dom";

const SelectCountry = () => {

    const [country, setCountry] = useState("")
    const [gender, setGender] = useState(localStorage.getItem("gender"))
    const navigate = useNavigate();
    const { search } = useLocation();

    useEffect(() => {
        setCountry(window.localStorage.getItem('country'));
        //else change the value of country to the value of the local storage
    }, [country]);


    return (
        <div>
            <select
                id="country"
                name="country"
                className=" mt-2 block w-42 rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                value={country} onChange={(e) => {setCountry(e.target.value)
                localStorage.setItem('country', e.target.value)}}>
                >
                <option></option>
                <option value="de">Germany</option>
                <option value="fr">France</option>
            </select>
        </div>
    );
};

export default SelectCountry;