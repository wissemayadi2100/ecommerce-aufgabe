import {Fragment, useEffect, useState} from 'react'
import { Disclosure, Menu, Transition } from '@headlessui/react'
import {Link, useLocation} from "react-router-dom";
import SelectCountry from "../CountryList/selectCountry";


export default function Navigation() {
    const [queryParams, setQueryParams] = useState({});
    const [country, setCountry] = useState();
    const location = useLocation();

    useEffect(() =>
    {
        const searchParams = new URLSearchParams(location.search);
        const params = {};

        for (const [key, value] of searchParams.entries())
        {
            params[key] = value;
        }
        setQueryParams(params);
    }, [location.search]);

    useEffect(() =>
    {
        setCountry(window.localStorage.getItem('country'));

    }, [country]);

    return (
        <Disclosure as="nav" className="bg-gray-800 fixed w-full z-10 top-0 p-1 mt-0 ">
                <div>
                    <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
                        <div className="relative flex h-16 items-center justify-between">

                            <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
                                <div className="flex flex-shrink-0 items-center">
                                    <img
                                        className="block h-8 w-auto lg:hidden"
                                        src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=500"
                                        alt="Your Company"
                                    />
                                    <img
                                        className="hidden h-8 w-auto lg:block"
                                        src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=500"
                                        alt="Your Company"
                                    />
                                </div>
                                <div className="hidden sm:ml-6 sm:block">
                                    <div className="flex space-x-4">
                                        <span className=" text-white rounded-md px-3 py-2 text-sm font-medium"
                                           aria-current="page">
                                            <Link to={{ pathname: '/men', search: `?country=${queryParams.country}&gender=${queryParams.gender}`}}>Men</Link></span>
                                        <span className=" text-white rounded-md px-3 py-2 text-sm font-medium" aria-current="page">
                                            <Link to={{ pathname: '/women', search: `?country=${queryParams.country}&gender=${queryParams.gender}`}}>Women</Link></span>
                                        <span className=" text-white rounded-md px-3 py-2 text-sm font-medium" aria-current="page">
                                            <Link to={{ pathname: '/allproduct', search: `?country=${queryParams.country}`}}>all product</Link></span>
                                    </div>
                                </div>
                            </div>
                            <div className="items-end justify-end mx-auto">
                                <SelectCountry/>
                            </div>
                        </div>
                    </div>
                </div>
        </Disclosure>

    )
}
