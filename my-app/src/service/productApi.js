import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import {createEntityAdapter, createSelector} from "@reduxjs/toolkit";



export const productApi = createApi({
    reducerPath: 'products',
    tagTypes: ['Products'],

    baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:3600/' }),

    endpoints: (builder) => ({
        getProductsByFilter: builder.query({
            query: (arg) => {
                const {country,gender,web_category} = arg;
                const queryParams = new URLSearchParams({
                    country,
                    gender,
                    web_category: web_category
                });
                return {
                    url: `filter?${queryParams.toString()}`,
                }
            },

            providesTags: ['Product'],
        }),


        getProductIdByCountry: builder.query({
            query: (arg) => {
                const {productId, country} = arg;
                return {
                    url: `products/${productId}?country=${country}`
                }}
            }),

        getImage: builder.query({
            query: (key,resolution,frame) => `images/${key}?res=${resolution}&frame=${frame}`,
            transformResponse: (response) => ({
                dataURL: URL.createObjectURL(response),
            }),
        }),
        getAllProducts: builder.query({
            query: (arg) => {
                const {country,gender,page=1} = arg;
                return {
                    url : `filter?country=${country}`
                }
            },
            providesTags: ['Product']
        }),

    }),

})
export const  {useGetProductIdByCountryQuery,
    useGetProductsByFilterQuery,
    useGetProductImageByIdQuery,
    useGetAllProductsQuery,
} = productApi;




