const express = require('express');
const app = express();
const axios = require('axios');
const cors = require('cors');
const PORT = process.env.PORT || 3600;
app.use(cors());

//DESC: Get product by id and country
//ROUTE: http://localhost:3600/products/01.06.356.0087?country=de
app.get('/products/:productId', async (req, res) => {
    const { productId } = req.params;
    const { country } = req.query;
    try {
        const response = await axios.get(`http://api.newyorker.de/csp/products/public/product/${productId}?country=${country}`);
        const product = response.data;
        res.json(product);
        console.log(product)
    } catch (error) {
        console.error(error);
        res.status(500).send('An error occurred while fetching the product data.');
    }
});
//DESC: Get product image
//ROUTE: http://localhost:3600/image/e9110f0fce29ecf1e9d59a29ff31316f.jpg?res=high&frame=2_3
//ACCESS: Public
app.get('/images/:key', async (req, res) => {
    const { key } = req.params;
    const { resolution, frame } = req.query;
    const apiUrl = `http://api.newyorker.de/csp/images/image/public/${key}?res=${resolution}&frame=${frame}`;

    try {
        const response = await axios.get(apiUrl, {
            responseType: 'arraybuffer'
        });

        const contentType = response.headers['content-type'];
        res.setHeader('Content-Type', contentType);
        res.send(response.data);
    } catch (error) {
        console.error(error);
        res.status(error.response.status || 500).send(error.response.data);
    }
});


//@http://localhost:3600/filter?country=de&gender=FEMALE&web_category=Accessoires,WCA01156,WCA01159,WCA01155,WCA01152,WCA01158,WCA01153,WCA01157,WCA01154
//@route GET /filterForm
//@access Public
app.get('/filter', async (req, res) => {
    try {
        const gender = req.query.gender;
        const country = req.query.country;
        const web_category = req.query.web_category;

        const response = await axios.get(`http://api.newyorker.de/csp/products/public/query`, {
            params: {
                'filters[country]': country,
                'filters[gender]': gender,
                'filters[web_category]': web_category
            }
        }
        );
      res.json(response.data);
    } catch (error) {
        console.error(error);
        res.status(500).send({ error: 'Internal Server Error', message: error.message });

    }
});



app.listen(PORT, () => {
        console.log(`Server running on port ${PORT}`);
    });





