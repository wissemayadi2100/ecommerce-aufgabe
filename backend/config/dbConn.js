const mongoose = require('mongoose')
require('dotenv').config({path:'./.env'})
const connectDB = async () => {
    try {
        mongoose.set('strictQuery', true);
        await mongoose.connect(process.env.DATABASE_URI,{
            useNewUrlParser: true,
            useUnifiedTopology: true
        })


        console.log("Data base connected")
    } catch (error) {

        console.log("Data base connection failed", error)
    }
}

module.exports=connectDB